import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {InputModule} from "../../../openaireLibrary/sharedComponents/input/input.module";
import {SettingsComponent} from "./settings.component";
import {BreadcrumbsModule} from "../../../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {IconsModule} from "../../../openaireLibrary/utils/icons/icons.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    ReactiveFormsModule, InputModule, BreadcrumbsModule,
    IconsModule
  ],
  declarations: [
    SettingsComponent
  ],
  providers: [],
  exports: [
    SettingsComponent
  ]
})
export class SettingsModule {}
