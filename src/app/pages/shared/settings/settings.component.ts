import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {Option} from "../../../openaireLibrary/sharedComponents/input/input.component";
import {StringUtils} from "../../../openaireLibrary/utils/string-utils.class";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.less']
})
export class SettingsComponent implements OnInit {
  @Input() single: boolean = true;
  @Input() sets: Option[] = [{label: 'All sets', value: 'all'}];
  @Output() validationClicked  = new EventEmitter();
  @Output() getSetsClicked  = new EventEmitter();

  @ViewChild("right_sidebar") right_sidebar;
  @ViewChild("right_sidebar_header") right_sidebar_header;
  @ViewChild("right_sidebar_footer") right_sidebar_footer;

  public right_sidebar_body_height: number = 0;
  public right_sidebar_card_height: number = 0;
  public offset: number = 0;
  public help: boolean = false;

  public form: UntypedFormGroup;
  public recordsNum: number = 10;
  @ViewChild('customRecordsNum') customRecordsNum;

  public options: Option[] = [
    {label: 'OpenAIRE Guidelines for Data Archives Profile v2 & OpenAIRE FAIR Guidelines for Data Repositories Profile', value: 'OpenAIRE Guidelines for Data Archives Profile v2'},
    {label: 'OpenAIRE Guidelines for Literature Repositories Profile v3', value: 'OpenAIRE Guidelines for Literature Repositories Profile v3'},
    {label: 'OpenAIRE Guidelines for Literature Repositories Profile v4 & OpenAIRE FAIR Guidelines for Literature Profile', value: 'OpenAIRE Guidelines for Literature Repositories Profile v4'},
    {label: 'OpenAIRE FAIR Guidelines for Data Repositories Profile', value: 'OpenAIRE FAIR Guidelines for Data Repositories Profile'}
  ];

  constructor(private fb: UntypedFormBuilder, private router: Router,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    if(this.single) {
      this.form = this.fb.group({
        guidelines: this.fb.control("", Validators.required),
        xml: this.fb.control('', Validators.required)
      });
    } else {
      this.form = this.fb.group({
        url: this.fb.control("", [Validators.required, StringUtils.urlValidator()]),//[Validators.required/*, Validators.email*/]),
        guidelines: this.fb.control("", Validators.required),
        recordsNum: this.fb.control(this.recordsNum, Validators.required),
        set: this.fb.control('all', Validators.required)
      });
    }
  }

  ngAfterViewInit() {
    if (typeof document !== 'undefined') {
      if(document.getElementById("main-menu")) {
        this.offset = Number.parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height'));
      } else {
        this.offset = 0;
      }
    }
  }

  ngOnDestroy() {}

  openHelp() {
    this.help = true;
    this.cdr.detectChanges();
    if (typeof document !== 'undefined') {
      this.right_sidebar_card_height = this.right_sidebar.nativeElement.offsetHeight - this.right_sidebar_header.nativeElement.offsetHeight + 1;
      this.right_sidebar_body_height = this.right_sidebar_card_height - this.right_sidebar_footer.nativeElement.offsetHeight;
    }
  }

  updateRecordsNum(increase: boolean = true) {
    this.recordsNum = this.recordsNum + (increase ? 10 : -10);
    this.form.get('recordsNum').setValue(this.recordsNum);
    if(this.customRecordsNum) {
      this.customRecordsNum.nativeElement.checked = true;
    }
  }

  getSets() {
    this.getSetsClicked.emit(this.form);
  }

  validate() {
    this.validationClicked.emit(this.form);
  }
}
