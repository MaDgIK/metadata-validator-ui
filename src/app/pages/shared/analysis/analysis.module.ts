import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {InputModule} from "../../../openaireLibrary/sharedComponents/input/input.module";
import {AnalysisComponent} from "./analysis.component";
import {BreadcrumbsModule} from "../../../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {AlertModalModule} from "../../../openaireLibrary/utils/modal/alertModal.module";
import {IconsModule} from "../../../openaireLibrary/utils/icons/icons.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    ReactiveFormsModule, InputModule, BreadcrumbsModule,
    AlertModalModule, IconsModule,
  ],
  declarations: [
    AnalysisComponent
  ],
  providers: [],
  exports: [
    AnalysisComponent
  ]
})
export class AnalysisModule {}
