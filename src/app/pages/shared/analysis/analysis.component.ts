import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {JobResult} from "../../entities/JobResult";
import {RulePerJob, Status} from "../../entities/RulePerJob";
import {Issue} from "../../entities/Issue";
import {Breadcrumb} from "../../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";

export class Duration {
  years: number;
  months: number;
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
}

@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.less']
})
export class AnalysisComponent {
  @Input() single: boolean = true;
  @Input() jobResult: JobResult = null;
  @Input() jobDuration: Duration = null;
  @Input() validationResult: Map<string, { "analysisResult": RulePerJob[], "successfulAnalysisResult": RulePerJob[], "warningAnalysisResult": RulePerJob[], "failedAnalysisResult": RulePerJob[] }>;
  @Input() breadcrumbs: Breadcrumb[];

  @Input() warnings: Issue[];
  @Input() errors: Issue[];
  @Input() internal_error: string;
  @Output() warningsChange = new EventEmitter();
  @Output() errorsChange = new EventEmitter();
  @Output() internal_errorChange = new EventEmitter();
  @Output() getWarningsClicked  = new EventEmitter();
  @Output() getErrorsClicked  = new EventEmitter();

  public warningsModalOpen: boolean = false;
  public errorsModalOpen: boolean = false;
  public xmlModalOpen: boolean = false;

  @ViewChild('warningsModal') warningsModal;
  @ViewChild('errorsModal') errorsModal;
  @ViewChild('xmlModal') xmlModal;

  public offset: number;

  public guidelinesLabelToPrefix: Map<string, string> = new Map([
    ['OpenAIRE Guidelines for Literature Repositories Profile v4', 'oai_openaire'],
    ['OpenAIRE Guidelines for Literature Repositories Profile v3', 'oai_dc'],
    ['OpenAIRE Guidelines for Data Archives Profile v2', 'oai_datacite'],
    ['OpenAIRE FAIR Guidelines for Data Repositories Profile', 'oai_datacite']
  ]);

  public requirementLevelMapping: Map<string, string> = new Map([
    ["MANDATORY", "M"],
    ["MANDATORY_IF_APPLICABLE", "MA"],
    ["RECOMMENDED", "R"],
    ["OPTIONAL", "O"]
  ]);


  constructor() {}

  ngAfterViewInit() {
    if (typeof document !== 'undefined') {
      if (document.getElementById("main-menu")) {
        this.offset = Number.parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height'));
      } else {
        this.offset = 0;
      }
    }
  }

  public openWarningsModal(rule: RulePerJob) {
    this.warningsModalOpen = true;
    this.warningsModal.cancelButton = false;
    this.warningsModal.okButton = false;
    this.warningsModal.alertTitle = rule.ruleName;
    this.warningsModal.open();
  }

  public openErrorsModal(rule: RulePerJob) {
    this.errorsModalOpen = true;
    this.errorsModal.cancelButton = false;
    this.errorsModal.okButton = false;
    this.errorsModal.alertTitle = rule.ruleName;
    this.errorsModal.open();
  }

  public openXmlModal() {
    this.xmlModalOpen = true;
    this.xmlModal.cancelButton = false;
    this.xmlModal.okButton = false;
    this.xmlModal.alertTitle = "Xml metadata record";
    this.xmlModal.open();
  }

  public getWarnings(rule: RulePerJob) {
    this.warnings = [];
    this.warningsChange.emit([]);
    this.openWarningsModal(rule);
    this.warningsModalOpen = true;
    this.getWarningsClicked.emit(rule);
  }

  public getErrors(rule: RulePerJob) {
    this.errors = [];
    this.errorsChange.emit([]);
    this.internal_error = "";
    this.internal_errorChange.emit("");
    this.openErrorsModal(rule);
    this.errorsModalOpen = true;
    this.getErrorsClicked.emit(rule);
  }

  public get hasDuration() {
    if(this.jobDuration && (this.jobDuration.years || this.jobDuration.months || this.jobDuration.days || this.jobDuration.hours || this.jobDuration.minutes || this.jobDuration.seconds)) {
      return true;
    }
    return false;
  }

  public getKeys(map) {
    return Array.from(map.keys());
  }

  public getValues(map) {
    return Array.from(map.values());
  }

  protected readonly Status = Status;
}
