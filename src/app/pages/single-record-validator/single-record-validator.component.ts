import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup} from "@angular/forms";
import {SingleRecordValidatorService} from "../../services/single-record-validator.service";
import {Option} from "../../openaireLibrary/sharedComponents/input/input.component";
import {RulePerJob, Status} from "../entities/RulePerJob";
import {JobResult, Progress} from "../entities/JobResult";
import {Breadcrumb} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {Issue} from "../entities/Issue";
import {Subscriber} from "rxjs";
import {NavigationEnd, Router} from "@angular/router";
import {Duration} from "../shared/analysis/analysis.component";

@Component({
  selector: 'app-single-record-validator',
  templateUrl: './single-record-validator.component.html'
})

export class SingleRecordValidatorComponent implements OnInit {
  public options: Option[] = [
    {label: 'OpenAIRE Guidelines for Data Archives Profile v2 & OpenAIRE FAIR Guidelines for Data Repositories Profile', value: 'OpenAIRE Guidelines for Data Archives Profile v2'},
    {label: 'OpenAIRE Guidelines for Literature Repositories Profile v3', value: 'OpenAIRE Guidelines for Literature Repositories Profile v3'},
    {label: 'OpenAIRE Guidelines for Literature Repositories Profile v4 & OpenAIRE FAIR Guidelines for Literature Profile', value: 'OpenAIRE Guidelines for Literature Repositories Profile v4'},
    {label: 'OpenAIRE FAIR Guidelines for Data Repositories Profile', value: 'OpenAIRE FAIR Guidelines for Data Repositories Profile'}
  ];

  public breadcrumbs: Breadcrumb[] = [{name: 'Single Record Validation', route: '/single-record-validate'}, {name: 'Result for ...'}];

  public form: UntypedFormGroup;
  public result: RulePerJob[];
  public jobResult: JobResult = null;
  public jobDuration: Duration = null;

  public validationResult: Map<string, { "analysisResult": RulePerJob[], "successfulAnalysisResult": RulePerJob[], "warningAnalysisResult": RulePerJob[], "failedAnalysisResult": RulePerJob[] }>;

  public viewResults: boolean = false;

  subscriptions = [];

  public warningsPerRule: Map<string, Issue[]>;
  public errorsPerRule: Map<string, Issue[]>;
  public warnings: Issue[];
  public errors: Issue[];
  public internal_error: string;

  constructor(private _router: Router, private fb: UntypedFormBuilder, private cdr: ChangeDetectorRef,
              private validator: SingleRecordValidatorService) {}

  ngOnInit(): void {
    this.subscriptions.push(this._router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // do some logic again when same url is clicked
        this.viewResults = false;
        this.result = [];
        this.jobResult = null;
        this.jobDuration = null;
        this.validationResult = null;
      }
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  public validate(form) {
    this.viewResults = true;
    this.jobResult = new JobResult();
    this.jobResult.startDate = new Date();
    this.jobResult.progress = Progress.IN_PROGRESS;
    this.jobResult.guidelines = form.get("guidelines").getRawValue();
    this.jobResult.xml = form.get("xml").getRawValue();

    this.subscriptions.push(this.validator.validateRecord(form.get('xml')?.getRawValue(), form.get('guidelines')?.getRawValue()).subscribe(
      result => {
        this.jobResult.endDate = new Date();
        this.jobResult.numberOfRecords = 1;
        this.jobResult.recordsTested = 1;
        this.jobResult.progress = Progress.COMPLETED;

        this.jobDuration = new Duration();
        const msBetweenDates = this.jobResult.endDate.getTime() - this.jobResult.startDate.getTime();
        this.jobDuration.seconds = Math.ceil(msBetweenDates / 1000 % 60);
        this.jobDuration.minutes = Math.floor(msBetweenDates / 1000 / 60 % 60);
        this.jobDuration.hours = Math.floor(msBetweenDates / 1000 / 60 / 60 % 24);
        this.jobDuration.days = Math.floor(msBetweenDates / 1000 / 60 / 60 / 24);
        this.jobDuration.months = Math.floor(this.jobDuration.days / 31);
        this.jobDuration.years = Math.floor(this.jobDuration.months / 12);

        this.result = result.summaryResults;
        let validationResult: Map<string, { "analysisResult": RulePerJob[], "successfulAnalysisResult": RulePerJob[], "warningAnalysisResult": RulePerJob[], "failedAnalysisResult": RulePerJob[] }> =
          new Map<string, {"analysisResult": RulePerJob[], "successfulAnalysisResult": RulePerJob[], "warningAnalysisResult": RulePerJob[], "failedAnalysisResult": RulePerJob[] }>;

        for(let rulePerJob of result.summaryResults) {
          if(rulePerJob.fair_principles) {
            if(rulePerJob.fair_principles.includes("F")) {
              rulePerJob.fair_principles_tooltip = "Findable";
            }
            if(rulePerJob.fair_principles.includes("F") &&
              (rulePerJob.fair_principles.includes("A") || rulePerJob.fair_principles.includes("I") || rulePerJob.fair_principles.includes("R"))) {
              rulePerJob.fair_principles_tooltip += ", ";
            }
            if(rulePerJob.fair_principles.includes("A")) {
              rulePerJob.fair_principles_tooltip += "Accessible";
            }
            if(rulePerJob.fair_principles.includes("A") &&
              (rulePerJob.fair_principles.includes("I") || rulePerJob.fair_principles.includes("R"))) {
              rulePerJob.fair_principles_tooltip += ", ";
            }
            if(rulePerJob.fair_principles.includes("I")) {
              rulePerJob.fair_principles_tooltip += "Interoperable";
            }
            if(rulePerJob.fair_principles.includes("I") && rulePerJob.fair_principles.includes("R")) {
              rulePerJob.fair_principles_tooltip += ", ";
            }
            if(rulePerJob.fair_principles.includes("R")) {
              rulePerJob.fair_principles_tooltip += "Reusable";
            }
          }

          if(rulePerJob.issues) {
            for(let issue of rulePerJob.issues) {
              if(issue.issueType == "WARNING") {
                if(this.warningsPerRule == null) {
                  this.warningsPerRule = new Map<string, Issue[]>();
                }
                if(!this.warningsPerRule.has(issue.ruleName)) {
                  this.warningsPerRule.set(issue.ruleName, new Array<Issue>());
                }
                this.warningsPerRule.get(issue.ruleName).push({description: issue.issueText, records: null});
              } else if(issue.issueType == "ERROR") {
                if(this.errorsPerRule == null) {
                  this.errorsPerRule = new Map<string, Issue[]>();
                }
                if(!this.errorsPerRule.has(issue.ruleName)) {
                  this.errorsPerRule.set(issue.ruleName, new Array<Issue>());
                }
                this.errorsPerRule.get(issue.ruleName).push({description: issue.issueText, records: null});
              }
            }
          }

          if(!validationResult.has(rulePerJob.guidelines)) {
            validationResult.set(rulePerJob.guidelines, {analysisResult: [], successfulAnalysisResult: [], warningAnalysisResult: [], failedAnalysisResult: []});
          }

          validationResult.get(rulePerJob.guidelines).analysisResult.push(rulePerJob);

          if(rulePerJob.rule_status == Status.FAILURE || rulePerJob.rule_status == Status.ERROR) {
            validationResult.get(rulePerJob.guidelines).failedAnalysisResult.push(rulePerJob);
          } else {
            if(rulePerJob.has_warnings || rulePerJob.has_errors || rulePerJob.internal_error) {
              validationResult.get(rulePerJob.guidelines).warningAnalysisResult.push(rulePerJob);
            } else {
              validationResult.get(rulePerJob.guidelines).successfulAnalysisResult.push(rulePerJob);
            }
          }
        }
        this.validationResult = validationResult;
        // this.cdr.detectChanges();

      }
    ));
  }

  // fileChangeEvent(fileInput: any, dropped: boolean = false) {
  //   if(this.form.value.value) {
  //     this.onRemove(false);
  //   }
  //
  //   if(dropped) {
  //     this.filesToUpload = fileInput.addedFiles;
  //   } else {
  //     this.filesToUpload = fileInput.target.files;
  //   }
  //
  //
  //   let messages: string[] = [];
  //   if (this.filesToUpload.length == 0) {
  //     messages.push(this.language.instant('DATASET-WIZARD.MESSAGES.NO-FILES-SELECTED'));
  //     return;
  //   } else {
  //     let fileToUpload = this.filesToUpload[0];
  //     if (this.form.get("data") && this.form.get("data").value.types
  //       && this.form.get("data").value.types.map(type => type.value).includes(fileToUpload.type)
  //       && this.form.get("data").value.maxFileSizeInMB
  //       && this.form.get("data").value.maxFileSizeInMB*1048576 >= fileToUpload.size) {
  //       this.upload();
  //     } else {
  //       this.filesToUpload = null;
  //       messages.push(this.language.instant('DATASET-WIZARD.MESSAGES.LARGE-FILE-OR-UNACCEPTED-TYPE'));
  //       messages.push(this.language.instant('DATASET-WIZARD.MESSAGES.MAX-FILE-SIZE', {'maxfilesize': this.form.get("data").value.maxFileSizeInMB}));
  //       messages.push(this.language.instant('DATASET-WIZARD.MESSAGES.ACCEPTED-FILE-TYPES')+ this.form.get("data").value.types.map(type => type.value).join(", "));
  //     }
  //
  //     if(messages && messages.length > 0) {
  //       this.dialog.open(FormValidationErrorsDialogComponent, {
  //         data: {
  //           errorMessages: messages
  //         }
  //       })
  //     }
  //   }
  // }

  // onRemove(makeFilesNull: boolean = true) {
  //   // delete from tmp folder - subscribe call
  //   this.fileService.deleteFromTempFolder(this.form.value.value.id).subscribe(res => {
  //     if(makeFilesNull) {
  //       this.makeFilesNull();
  //     }
  //     this.cdr.detectChanges();
  //   }, error => {
  //     if(makeFilesNull) {
  //       this.makeFilesNull();
  //     }
  //   })
  // }
  //
  // makeFilesNull() {
  //   this.filesToUpload = null;
  //   this.form.value.value = null;
  //   this.form.get("value").patchValue(null);
  // }

  getWarnings(rule: RulePerJob) {
    this.warnings = [];
    if(this.warningsPerRule == null) {
      this.warnings = [];
    } else {
      this.warnings = [...this.warningsPerRule.get(rule.ruleName)];
    }
  }

  getErrors(rule: RulePerJob) {
    this.internal_error = rule.internal_error;

    this.errors = [];
    if(this.errorsPerRule == null) {
      this.errors = [];
    } else {
      this.errors = [...this.errorsPerRule.get(rule.ruleName)];
    }
  }
}
