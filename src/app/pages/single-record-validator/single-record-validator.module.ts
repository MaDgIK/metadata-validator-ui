import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {SingleRecordValidatorRoutingModule} from "./single-record-validator-routing.module";
import {SingleRecordValidatorComponent} from "./single-record-validator.component";
import {SingleRecordValidatorService} from "../../services/single-record-validator.service";
import {AnalysisModule} from "../shared/analysis/analysis.module";
import {SettingsModule} from "../shared/settings/settings.module";

@NgModule({
  imports: [
    CommonModule, RouterModule,
    SingleRecordValidatorRoutingModule, AnalysisModule, SettingsModule
  ],
  declarations: [
    SingleRecordValidatorComponent
  ],
  providers: [
    SingleRecordValidatorService
  ],
  exports: [
    SingleRecordValidatorComponent
  ]
})
export class SingleRecordValidatorModule {}
