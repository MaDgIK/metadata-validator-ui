import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {PreviousRouteRecorder} from "../../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import { SingleRecordValidatorComponent } from "./single-record-validator.component";
@NgModule({
  imports: [
    RouterModule.forChild([{ path: '', component: SingleRecordValidatorComponent, canDeactivate: [PreviousRouteRecorder] }])
  ]
})
export class SingleRecordValidatorRoutingModule { }
