import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {OaipmhHistoryRoutingModule} from "./oaipmh-history-routing.module";
import {OaipmhHistoryComponent} from "./oaipmh-history.component";
import {BreadcrumbsModule} from "../../../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {AlertModalModule} from "../../../openaireLibrary/utils/modal/alertModal.module";

@NgModule({
  imports: [
    CommonModule, RouterModule,
    OaipmhHistoryRoutingModule,
    BreadcrumbsModule, AlertModalModule
  ],
  declarations: [
    OaipmhHistoryComponent
  ],
  providers: [],
  exports: [
    OaipmhHistoryComponent
  ]
})
export class OaipmhHistoryModule {}
