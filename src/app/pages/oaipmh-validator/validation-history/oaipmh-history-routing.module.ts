import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PreviousRouteRecorder } from "../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import { OaipmhHistoryComponent } from "./oaipmh-history.component";
@NgModule({
  imports: [
    RouterModule.forChild([{ path: '', component: OaipmhHistoryComponent, canDeactivate: [PreviousRouteRecorder] }])
  ]
})
export class OaipmhHistoryRoutingModule { }
