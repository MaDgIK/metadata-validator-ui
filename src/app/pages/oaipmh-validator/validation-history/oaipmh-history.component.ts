import {Component, OnInit, ViewChild} from '@angular/core';
import {OaipmhValidatorService} from "../../../services/oaipmh-validator.service";
import {JobResult, Progress, Status} from "../../entities/JobResult";
import {ActivatedRoute} from "@angular/router";
import {Subscriber} from "rxjs";
import {Breadcrumb} from "../../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {filter, repeat, take} from "rxjs/operators";

@Component({
  selector: 'app-oaipmh-history',
  templateUrl: './oaipmh-history.component.html',
  styleUrls: ['./oaipmh-history.component.less']
})
export class OaipmhHistoryComponent implements OnInit {
  public warning: boolean = false;
  public results: JobResult[];
  public jobId: string = "";
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'Validator\'s History'}];
  protected readonly Progress = Progress;
  protected readonly Status = Status;

  @ViewChild('errorsModal') errorsModal;
  public errorsModalOpen: boolean = false;
  public error: string = null;

  subscriptions = [];

  constructor(private route: ActivatedRoute, private validator: OaipmhValidatorService) {}

  ngOnInit(): void {
    this.subscriptions.push(this.route.queryParams.subscribe(params => {
      this.jobId = params['jobId'];
      this.results = [];
      this.warning = false;
      if(!this.jobId) {
        this.getJobResults();
      } else {
        this.getJobResult();
      }
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  public getJobResult() {
    let count: number = 0;
    this.subscriptions.push(this.validator.getJobResult(this.jobId)
      .pipe(
        repeat({ delay: 5000 }),
        filter((result: JobResult) => {
          this.results = [];
          this.results.push(result);
          count++;
          if(count == 120 && result.progress == Progress.IN_PROGRESS) {
            this.warning = true;
            return true;
          }
          return result.progress !== Progress.IN_PROGRESS
        }),
        take(1)
      )
      .subscribe()
    );
  }

  public getJobResults() {
    this.subscriptions.push(this.validator.getJobResults().subscribe(
      (results: JobResult[]) => {
        this.results = results;
      }),
    );
  }

  public viewErrors(error: string) {
    this.error = error;
    this.openErrorsModal();
    this.errorsModalOpen = true;
  }

  public openErrorsModal() {
    this.errorsModalOpen = true;
    this.errorsModal.cancelButton = false;
    this.errorsModal.okButton = false;
    this.errorsModal.alertTitle = "Errors";
    this.errorsModal.open();
  }
}
