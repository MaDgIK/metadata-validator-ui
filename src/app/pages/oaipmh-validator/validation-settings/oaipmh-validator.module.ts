import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {OaipmhValidatorRoutingModule} from "./oaipmh-validator-routing.module";
import {OaipmhValidatorComponent} from "./oaipmh-validator.component";
import {SettingsModule} from "../../shared/settings/settings.module";

@NgModule({
  imports: [
    CommonModule, RouterModule,
    OaipmhValidatorRoutingModule, SettingsModule
  ],
  declarations: [
    OaipmhValidatorComponent
  ],
  providers: [],
  exports: [
    OaipmhValidatorComponent
  ]
})
export class OaipmhValidatorModule {}
