import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Subscriber} from "rxjs";
import {OaipmhValidatorService} from "../../../services/oaipmh-validator.service";
import {Option} from "../../../openaireLibrary/sharedComponents/input/input.component";
import {StringUtils} from "../../../openaireLibrary/utils/string-utils.class";
import {JobResult} from "../../entities/JobResult";

declare var UIkit: any;

@Component({
  selector: 'app-oaipmh-validator',
  templateUrl: './oaipmh-validator.component.html'
})

export class OaipmhValidatorComponent implements OnInit {
  public sets: Option[] = [{label: 'All sets', value: 'all'}];
  subscriptions = [];

  constructor(private router: Router,
              private cdr: ChangeDetectorRef,
              private validator: OaipmhValidatorService) {
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  getSets(form) {
    let options: Option[] = [{label: 'All sets', value: 'all'}];
    if(form.get('setName')) {
      form.get('setName').setValue('all');
    }
    if(form.get('url') && form.get('url').value && StringUtils.isValidUrl(form.get('url').value)) {
      this.subscriptions.push(this.validator.getSets(form.get('url').value).subscribe(sets => {
        for(let set of sets) {
          options.push({label: set['setName'], value: set['setSpec']});
        }
        this.sets = options;
      }, error => {
        this.sets = options;
      }));
    } else {
      this.sets = options;
    }
  }

  validate(form) {
    this.subscriptions.push(this.validator.validate(form.get('guidelines').value, form.get('url').value, form.get('recordsNum').value, form.get('set').value).subscribe(
      (result: JobResult) => {
        this.router.navigate(['/oaipmh-history'], {
          queryParams: { 'jobId': result.id }
        });
      },
      error => {
        // if(error.status == 400) {
          UIkit.notification((error.error && error.error.message) ? error.error.message: error.message, {
            status: 'danger',
            timeout: 4000,
            pos: 'bottom-right'
          });
        // }
        // if(error.status == 422) {
          // this.router.navigate(['/oaipmh-history'], {
          //   queryParams: { 'jobId': error.error.id }
          // })
        // }
      }
    ));
  }
}
