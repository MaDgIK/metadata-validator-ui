import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PreviousRouteRecorder } from "../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import { OaipmhValidatorComponent } from "./oaipmh-validator.component";
@NgModule({
  imports: [
    RouterModule.forChild([{ path: '', component: OaipmhValidatorComponent, canDeactivate: [PreviousRouteRecorder] }])
  ]
})
export class OaipmhValidatorRoutingModule { }
