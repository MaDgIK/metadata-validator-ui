import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {OaipmhValidatorService} from "../../../services/oaipmh-validator.service";
import {Subscriber} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {RulePerJob, Status} from "../../entities/RulePerJob";
import {Issue} from "../../entities/Issue";
import {JobResult} from "../../entities/JobResult";
import {Breadcrumb} from "../../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {Duration} from "../../shared/analysis/analysis.component";

@Component({
  selector: 'app-oaipmh-analysis',
  templateUrl: './oaipmh-analysis.component.html'
})

export class OaipmhAnalysisComponent implements OnInit {
  public jobResult: JobResult = null;
  public jobDuration: Duration = null;
  public validationResult: Map<string, { "analysisResult": RulePerJob[], "successfulAnalysisResult": RulePerJob[], "warningAnalysisResult": RulePerJob[], "failedAnalysisResult": RulePerJob[] }>;
  public warnings: Issue[];
  public errors: Issue[];
  public internal_error: string;

  public jobId: string = "";
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'Validator\'s History', route: '/oaipmh-history'}, {name: 'Result for ...'}];

  subscriptions = [];

  constructor(private route: ActivatedRoute, private cdr: ChangeDetectorRef, private validator: OaipmhValidatorService) {}

  ngOnInit(): void {
    this.subscriptions.push(this.route.queryParams.subscribe(params => {
      this.jobId = params['jobId'];
      this.jobResult = null;
      if(this.jobId) {
        this.getAnalysis();
        this.getJobResult();
      }
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  public getAnalysis() {
    this.subscriptions.push(this.validator.getAnalysis(this.jobId).subscribe(
      (result: RulePerJob[]) => {
        // this.analysisResult = result;

        let validationResult: Map<string, { "analysisResult": RulePerJob[], "successfulAnalysisResult": RulePerJob[], "warningAnalysisResult": RulePerJob[], "failedAnalysisResult": RulePerJob[] }> =
          new Map<string, {"analysisResult": RulePerJob[], "successfulAnalysisResult": RulePerJob[], "warningAnalysisResult": RulePerJob[], "failedAnalysisResult": RulePerJob[] }>;

        for(let rulePerJob of result) {
          if(rulePerJob.fair_principles) {
            if(rulePerJob.fair_principles.includes("F")) {
              rulePerJob.fair_principles_tooltip = "Findable";
            }
            if(rulePerJob.fair_principles.includes("F") &&
              (rulePerJob.fair_principles.includes("A") || rulePerJob.fair_principles.includes("I") || rulePerJob.fair_principles.includes("R"))) {
              rulePerJob.fair_principles_tooltip += ", ";
            }
            if(rulePerJob.fair_principles.includes("A")) {
              rulePerJob.fair_principles_tooltip += "Accessible";
            }
            if(rulePerJob.fair_principles.includes("A") &&
              (rulePerJob.fair_principles.includes("I") || rulePerJob.fair_principles.includes("R"))) {
              rulePerJob.fair_principles_tooltip += ", ";
            }
            if(rulePerJob.fair_principles.includes("I")) {
              rulePerJob.fair_principles_tooltip += "Interoperable";
            }
            if(rulePerJob.fair_principles.includes("I") && rulePerJob.fair_principles.includes("R")) {
              rulePerJob.fair_principles_tooltip += ", ";
            }
            if(rulePerJob.fair_principles.includes("R")) {
              rulePerJob.fair_principles_tooltip += "Reusable";
            }
          }

          if(!validationResult.has(rulePerJob.guidelines)) {
            validationResult.set(rulePerJob.guidelines, {analysisResult: [], successfulAnalysisResult: [], warningAnalysisResult: [], failedAnalysisResult: []});
          }

          validationResult.get(rulePerJob.guidelines).analysisResult.push(rulePerJob);

          if(rulePerJob.rule_status == Status.FAILURE || rulePerJob.rule_status == Status.ERROR) {
            validationResult.get(rulePerJob.guidelines).failedAnalysisResult.push(rulePerJob);
          } else {
            if(rulePerJob.has_warnings || rulePerJob.has_errors || rulePerJob.internal_error) {
              validationResult.get(rulePerJob.guidelines).warningAnalysisResult.push(rulePerJob);
            } else {
              validationResult.get(rulePerJob.guidelines).successfulAnalysisResult.push(rulePerJob);
            }
          }
        }
        this.validationResult = validationResult;
        this.cdr.detectChanges();
      }
    ));
  }

  getWarnings(rule: RulePerJob) {
    this.warnings = [];
    this.subscriptions.push(this.validator.getWarnings(this.jobId, rule.ruleName).subscribe(
      result => {
        this.warnings = result;
      }
    ));
  }

  getErrors(rule: RulePerJob) {
    this.internal_error = rule.internal_error;
    this.errors = [];
    if(rule.has_errors) {
      this.subscriptions.push(this.validator.getErrors(this.jobId, rule.ruleName).subscribe(
        result => {
          this.errors = result;
        }
      ));
    }
  }

  public getJobResult() {
    this.subscriptions.push(this.validator.getJobResult(this.jobId).subscribe(
      (result: JobResult) => {
        this.jobResult = result;
        let startDate = new Date(this.jobResult.startDate);
        let endDate = this.jobResult.endDate ? new Date(this.jobResult.endDate) : new Date();

        this.jobDuration = new Duration();
        const msBetweenDates = endDate.getTime() - startDate.getTime();
        this.jobDuration.seconds = Math.ceil(msBetweenDates / 1000 % 60);
        this.jobDuration.minutes = Math.floor(msBetweenDates / 1000 / 60 % 60);
        this.jobDuration.hours = Math.floor(msBetweenDates / 1000 / 60 / 60 % 24);
        this.jobDuration.days = Math.floor(msBetweenDates / 1000 / 60 / 60 / 24);
        this.jobDuration.months = Math.floor(this.jobDuration.days / 31);
        this.jobDuration.years = Math.floor(this.jobDuration.months / 12);
      }
    ));
  }

  protected readonly Status = Status;
}
