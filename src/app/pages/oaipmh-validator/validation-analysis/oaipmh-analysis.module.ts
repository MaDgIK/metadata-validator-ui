import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {OaipmhAnalysisRoutingModule} from "./oaipmh-analysis-routing.module";
import {OaipmhAnalysisComponent} from "./oaipmh-analysis.component";
import {AnalysisModule} from "../../shared/analysis/analysis.module";

@NgModule({
  imports: [
    CommonModule, RouterModule,
    OaipmhAnalysisRoutingModule, AnalysisModule
  ],
  declarations: [
    OaipmhAnalysisComponent
  ],
  providers: [],
  exports: [
    OaipmhAnalysisComponent
  ]
})
export class OaipmhAnalysisModule {}
