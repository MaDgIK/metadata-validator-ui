import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PreviousRouteRecorder } from "../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import { OaipmhAnalysisComponent } from "./oaipmh-analysis.component";
@NgModule({
  imports: [
    RouterModule.forChild([{ path: '', component: OaipmhAnalysisComponent, canDeactivate: [PreviousRouteRecorder] }])
  ]
})
export class OaipmhAnalysisRoutingModule { }
