export class JobResult {
  id: string;
  xml?: string;
  baseUrl: string;
  set: string;
  numberOfRecords: number;
  guidelines: string;
  startDate: Date;
  endDate: Date;
  recordsTested: number;
  progress: Progress;
  status: Status;
  score: number;
  exceptionMessage: string;
}

export enum Status {
  SUCCESS = "SUCCESS",
  FAILURE = "FAILURE"
}

export enum Progress {
  IN_PROGRESS = "IN_PROGRESS",
  COMPLETED = "COMPLETED",
  STOPPED = "STOPPED"
}
