import {RuleInfo} from "./RuleInfo";

export class XmlValidationResponse {
  validationScore: string;
  fairScore: string;
  rules: RuleInfo[];
  fairRules: RuleInfo[];
}
