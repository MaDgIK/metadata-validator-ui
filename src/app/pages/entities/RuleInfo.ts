export class RuleInfo {
  name: string;
  description: string;
  warnings: string[];
  errors: string[];
  internalError: string;
  status: Status;
  score: number;
}

export enum Status {
  SUCCESS,
  FAILURE,
  ERROR
}
