export class RulePerJob {
  ruleName: string;
  rule_weight: number;
  guidelines: string;
  rule_status: Status;

  "requirement_level": string;
  "description": string;
  "fair_principles": string;
  "fair_principles_tooltip": string;
  "link": string;

  passed_records: number;
  failed_records: number;
  // warnings: string[];
  // errors: string[];
  internal_error: string;
  has_errors: boolean;
  has_warnings: boolean;
}

export enum Status {
  SUCCESS = "SUCCESS",
  FAILURE = "FAILURE",
  ERROR = "ERROR"
}
