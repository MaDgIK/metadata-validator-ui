import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class SingleRecordValidatorService {

  constructor(private http: HttpClient) {}

  validateRecord(xml: string, guidelinesName: string): Observable<any> {
    let url = environment.validatorAPI + "validateRecord?guidelines="+guidelinesName;
    let headers = new HttpHeaders({'Content-Type': 'application/json', 'accept': 'application/json'});
    return this.http.post<any>(url, xml, {headers: headers});
  }

  uploadAndValidateFile(file: FileList, guidelinesName: string): Observable<any> {
    let url = environment.validatorAPI + "validate-file";

    let headers = new HttpHeaders({'Content-Type': 'application/json', 'accept': 'application/json'});
    const formData = new FormData();
    formData.append('file', file[0]);
    formData.append('guidelines', guidelinesName);
    return this.http.post(url, formData, { headers: headers });
  }
}
