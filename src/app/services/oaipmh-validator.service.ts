import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {map} from "rxjs";

@Injectable({
  providedIn: "root"
})

export class OaipmhValidatorService {

  constructor(private http: HttpClient) {}

  getAnalysis(jobId: string) {
    let url: string = environment.validatorAPI + "reports/getSummaryFromDB?jobId="+jobId;
    return this.http.get(url);
  }

  getWarnings(jobId: string, ruleName: string) {
    let url: string = environment.validatorAPI + "reports/getWarningsReport?jobId="+jobId+"&ruleName="+ruleName;
    return this.http.get<any>(url);
  }

  getErrors(jobId: string, ruleName: string) {
    let url:string = environment.validatorAPI + "reports/getErrorsReport?jobId="+jobId+"&ruleName="+ruleName;
    return this.http.get<any>(url);
  }

  getJobResult(jobId: string) {
    let url: string = environment.validatorAPI + "reports/getJobResult?jobId="+jobId;
    return this.http.get(url);
  }

  getJobResults(limit: number = 200) {
    let url: string = environment.validatorAPI + "jobs/latest?limit="+limit;
    return this.http.get(url);
  }

  getSets(baseUrl) {
    let url: string = environment.validatorAPI + "getSets?baseUrl="+baseUrl;
    return this.http.get(url).pipe(map(res => res['set']));
  }

  validate(guidelines: string, baseUrl: string, numberOfRecords: string, set: string) {
    let url: string = environment.validatorAPI + "realValidator?guidelines="+guidelines+"&baseUrl="+baseUrl
      +(numberOfRecords ? ("&numberOfRecords="+numberOfRecords) : "") + (set && set != 'all' ? ("&set="+set) : "");
    console.log(url);
    return this.http.get(url);
  }
}
