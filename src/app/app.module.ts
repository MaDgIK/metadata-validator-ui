import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopmenuComponent } from './shared/topmenu/topmenu.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {CommonModule} from "@angular/common";
import {SingleRecordValidatorModule} from "./pages/single-record-validator/single-record-validator.module";
import {AnalysisModule} from "./pages/shared/analysis/analysis.module";
import {SettingsModule} from "./pages/shared/settings/settings.module";
import {OaipmhValidatorModule} from "./pages/oaipmh-validator/validation-settings/oaipmh-validator.module";
import {OaipmhHistoryModule} from "./pages/oaipmh-validator/validation-history/oaipmh-history.module";
import {OaipmhAnalysisModule} from "./pages/oaipmh-validator/validation-analysis/oaipmh-analysis.module";

@NgModule({
  declarations: [
    AppComponent,
    TopmenuComponent,
    SidebarComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SingleRecordValidatorModule,
    OaipmhValidatorModule,
    OaipmhHistoryModule,
    OaipmhAnalysisModule,
    AnalysisModule,
    SettingsModule
  ],
  providers: [],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
