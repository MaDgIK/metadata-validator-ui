import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'oaipmh'
  },
  {
    path: 'single-record-validate',
    loadChildren: () => import('./pages/single-record-validator/single-record-validator.module').then(m => m.SingleRecordValidatorModule)},
  {
    path: 'oaipmh-analysis',
    loadChildren: () => import('./pages/oaipmh-validator/validation-analysis/oaipmh-analysis.module').then(m => m.OaipmhAnalysisModule)
  },
  {
    path: 'oaipmh-history',
    loadChildren: () => import('./pages/oaipmh-validator/validation-history/oaipmh-history.module').then(m => m.OaipmhHistoryModule)
  },
  {
    path: 'oaipmh',
    loadChildren: () => import('./pages/oaipmh-validator/validation-settings/oaipmh-validator.module').then(m => m.OaipmhValidatorModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
    onSameUrlNavigation: "reload"
  }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
